import os,sys
import json
import matplotlib.pyplot as plt

import xobjects as xo
import xtrack as xt

from cpymad.madx import Madx as madx

#betaStars=[15,20,64,100]
betaStars=[20]
orbitBump=0
tcpBetaRetune=0
offsetAperture=0
fullOutput=0
mbhShift=0
maskb1='job_sample_thin.madx'
maskb4='job_sample_thin_b4.madx'


def run_mask(mask, outfile, beam, plot=False):
    mad = madx()
    mad.call(mask)
    if( beam == 'b1'):
        madSequence = mad.sequence.lhcb1
    else:
        madSequence = mad.sequence.lhcb2
    line = xt.Line.from_madx_sequence(madSequence, apply_madx_errors=False, install_apertures=True)

    if( plot ):
        twiss = madSequence.twiss_table
        plt.plot(twiss['s'], twiss['x'])
        plt.plot(twiss['s'], twiss['y'])
        plt.show()

    # save to JSON
    with open(outfile,'w') as fid:
        json.dump(line.to_dict(), fid, cls=xo.JEncoder)

    # load from JSON to check
    with open(outfile,'r') as fid:
        loaded_dct = json.load(fid)
    line_2 = xt.Line.from_dict(loaded_dct)


os.system("rm batchLog.txt") 
for betaStar in betaStars:
    if orbitBump == 1:
        str1 = "+ORBITBUMP"
    else:
        str1 = "-ORBITBUMP"
    if tcpBetaRetune == 1:
        str2 = "+TCPBETARETUNE"
    else:
        str2 = "-TCPBETARETUNE"
    if offsetAperture == 1:
        str3 = "+OFFSETAPER"
    else:
        str3 = "-OFFSETAPER"
    if mbhShift == 1:
        str4 = "TCLD"
    else:
        str4 = "NoTCLD"

    if orbitBump+tcpBetaRetune == 0:
        run='250muradXing'+str(betaStar)+'cmBeta'+str4+'_default'+str3
    else:
        run='250muradXing'+str(betaStar)+'cmBeta'+str4+str1+str2+str3
    if not os.path.exists('out/'+run):
        os.makedirs('out/'+run)
    os.system('cp jobFiles/job_sample* ./')
    os.system("sed -i 's/BETASTARSETTING/"+str(betaStar)+"/g' job_sample_thin_b4.madx") 
    os.system("sed -i 's/BETASTARSETTING/"+str(betaStar)+"/g' job_sample_thin.madx")
    os.system("sed -i 's/ORBITBUMPSWITCH/"+str(orbitBump)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/ORBITBUMPSWITCH/"+str(orbitBump)+"/g' job_sample_thin.madx") 
    os.system("sed -i 's/TCPBETARETUNESWITCH/"+str(tcpBetaRetune)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/TCPBETARETUNESWITCH/"+str(tcpBetaRetune)+"/g' job_sample_thin.madx")   
    os.system("sed -i 's/OFFSETAPERTURESWITCH/"+str(offsetAperture)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/OFFSETAPERTURESWITCH/"+str(offsetAperture)+"/g' job_sample_thin.madx")  
    os.system("sed -i 's/FULLOUTPUTSWITCH/"+str(fullOutput)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/FULLOUTPUTSWITCH/"+str(fullOutput)+"/g' job_sample_thin.madx")    
    os.system("sed -i 's/MBHSHIFT/"+str(mbhShift)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/MBHSHIFT/"+str(mbhShift)+"/g' job_sample_thin.madx")   
 
    run_mask(maskb1,'b1_sequence.json','b1',False)
    run_mask(maskb4,'b4_sequence.json','b4',False)

    os.system("grep IP5 out/LHC* >> batchLog.txt")

    os.system("mv out/LHC* out/"+run)
    if( fullOutput == 1):
        os.system("mv out/Surv* out/"+run)
    os.system("mv b*sequence.json out/"+run)
