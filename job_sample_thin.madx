option, warn,info;
system,"ln -fns /afs/cern.ch/eng/lhc/optics/runIII lhc";
system,"ln -fns /afs/cern.ch/eng/lhc/optics/HLLHCV1.5  slhc";
system,"ln -fns ../generate_aperture/extraTools myScripts"; ! link to aperture offset script
system,"ln -fns ../generate_aperture/processedFiles processedFiles"; ! link to layoutDB and aperture patching files, from generate_aperture scripts
option, -echo, -warn,info;
call,file="slhc/toolkit/macro.madx";
call,file="slhc/aperture/const_for_aperture.madx";
call,file="lhc/LHC_LS2_2021-07-02.seq";

!--------------------------------------------------
! OPTIONS
!--------------------------------------------------

betaStar=20; ! 15, 20, 64, 100
orbitBump=0; 
tcpBetaRetune=0;
offsetAperture=0;
fullOutput=0;

!-------------------------------------------------
!
!-------------------------------------------------

mylhcbeam = 1;
exec,mk_beam(7000);
call,file="slhc/hllhc_sequence.madx";   
call,file="lhc/aperture/aperture.b1.madx";
call,file="lhc/aperture/aperture.b2.madx";
call,file="lhc/aperture/aper_tol.b1.madx";
call,file="lhc/aperture/aper_tol.b2.madx";
call,file="slhc/aperture/exp_pipe_model_after_LS3.madx";
call,file="slhc/aperture/exp_pipe_install_after_LS3.madx";
call,file="slhc/aperture/aperture_upgrade_IT.madx";
call,file="slhc/aperture/aperture_upgrade_MS.madx";
call,file="processedFiles/layoutDB.seq";
call,file="processedFiles/patchApertures_b1_release_v06.madx";
call,file="processedFiles/fromCollimation_define.madx";
call,file="processedFiles/fromCollimation_install.madx";    

! Set custom variables for some MQs in case beta function rematch
if(tcpBetaRetune==1){
MQ.7L7.B1,    K1 := my1, polarity=-1;
MQ.8L7.B1,    K1 := my2, polarity=+1;
MQ.9L7.B1,    K1 := my3, polarity=-1;
MQ.10L7.B1,   K1 := my4, polarity=+1;
MQWA.E5L7.B1, K1 := my5, polarity=-1;
MQWA.D5L7.B1, K1 := my6, polarity=-1;
MQWA.C5L7.B1, K1 := my7, polarity=-1;
MQWA.B5L7.B1, K1 := my8, polarity=-1;
MQWA.A5L7.B1, K1 := my9, polarity=-1;
my1 := kqd.a67;
my2 := kqf.a67;
my3 := kqd.a67;
my4 := kqf.a67;
my5 := kq5.lr7;
my6 := kq5.lr7;
my7 := kq5.lr7;
my8 := kq5.lr7;
my9 := kq5.lr7;
};
 
seqedit,sequence=lhcb1;flatten;endedit;
seqedit,sequence=lhcb2;flatten;endedit;
call,file="processedFiles/correctMechSep3.madx";
mbh_shift=0; ! must set this to zero when not using MBH otherwise aperture will be wrong, this shift is due to MBH being RBEND
make_end_markers=1;   
exec,myslice;

!seqedit, sequence=lhcb1;cycle,start=IP1; endedit;
!seqedit, sequence=lhcb2;cycle,start=IP1; endedit;

if(betaStar==15){call,file="slhc/round/opt_round_150_1500_thin.madx";}
elseif(betaStar==20){call,file="slhc/round/opt_round_200_1500_tcdq4_thin.madx";}
elseif(betaStar==64){call,file="slhc/ramp/opt_ramp_640_1500_thin.madx";}
elseif(betaStar==100){call,file="slhc/ramp/opt_ramp_1000_1500_thin.madx";}

set,format="14.8f";
select, flag=twiss, CLEAR; 
select, flag=twiss,column=name,s,l,mux,betx,alfx,muy,bety,alfy,dx,dpx,dy,dpy,x,y,px,py,apertype;   
use,sequence=lhcb1;twiss, table=twiss;if(fullOutput==1){write,table=twiss,file="out/LHC_b1_7000GeV_thinNoXing.twiss";}  
use,sequence=lhcb2;twiss, table=twiss;if(fullOutput==1){write,table=twiss,file="out/LHC_b2_7000GeV_thinNoXing.twiss";}              

! Set the xing knobs
call, file="auxSettings/xingKnobs.madx";

! Rematch post-knobs
call, file="slhc/toolkit/rematch_chroma.madx";
call, file="slhc/toolkit/rematch_tune.madx";    
if(offsetAperture==1){
    call,file="myScripts/aperoffset_elements.madx"; ! add aperture offsets in the doglegs
}

! Rematch TCP beta functions and/or IR7 orbit bump (default off)
call, file="auxSettings/opticsRematching.madx";

select, flag=twiss, CLEAR; 
select, flag=twiss,column=name,keyword,s,l,mux,betx,alfx,muy,bety,alfy,dx,dpx,dy,dpy,x,y,px,py,apertype,mech_sep;   
use,sequence=lhcb2;twiss, centre=true, table=twiss;write,table=twiss,file="out/LHC_b2_7000GeV_thinXing.twiss";     
if(fullOutput==1){survey, sequence=lhcb2,file="out/SurveyWithCrossing_XP_lowb_b2.dat";}   
use,sequence=lhcb1;twiss, centre=true, table=twiss;write,table=twiss,file="out/LHC_b1_7000GeV_thinXing.twiss";  
if(fullOutput==1){survey, sequence=lhcb1,file="out/SurveyWithCrossing_XP_lowb_b1.dat";}   

system, "rm lhc slhc d2_aperture myScripts processedFiles"
if(offsetAperture==1){system, "rm survey_lhcb1.tfs";}  
